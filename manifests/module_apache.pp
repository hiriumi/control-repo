# include apache

# $user = 'www-data'

# user { 'user account to run the apache server as':
#   name     => $user,
#   password => 'hogehoge',
# }

# exec { 'echo':
#   command => "echo 'hello world'",
#   path    => '/usr/bin',
# }

# exec { 'firewall-cmd': 
#   command     => 'firewall-cmd --zone=public --add-port=1111/tcp --permanent',
#   path        => '/usr/bin',
#   refreshonly => true,
# }

# apache::vhost { 'cat-pictures.com':
#   port          => '1111',
#   docroot       => '/var/www/cat-pictures',
#   docroot_owner => $user,
#   docroot_group => $user,
# }

# file { '/var/www/cat-pictures/index.html':
#   content => "<img src='http://bitfieldconsulting.com/files/happycat.jpg'>",
#   owner   => $user,
#   group   => $user,

# }
